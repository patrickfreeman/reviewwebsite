<!DOCTYPE html>
<html>
  <head>
    <?php
    session_start();
    include 'includes/config.php';
    if(isset($_POST["submit"])){

    }
    ?>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="js/submitScript.js"></script>
	  <script src="js/validation.js"></script>
    <script src="js/geolocate.js"></script>
    <title>Park Search - Home</title>
  </head>
  <body>
    <div id="header">
        <?php include "includes/menu.inc";?>
    </div>
    <div id="wrapper">
      <div id="searchbox">
        <h1>Search by:</h1>
          <div class="searchSelect">
            <ul>
              <li><a href="#" onclick="suburbForm();">Suburb</a>
              <li><a href="#" onclick="nameForm();">Name</a>
              <li><a href="#" onclick="ratingForm();">Rating</a>
              <li><a href="#" onclick="geoForm();getLocation()">Geolocate</a>
            </ul>
            <br />

            <div class ="incorrectInput"><span id="errorSpan">test</span></div>
        </div>
        <!--Suburb Form -->
        <div id="suburbSearch">
          <form action="results.php" onsubmit="return checkSuburb();">
            <div class="searchboxText">
              <select id="suburb" name="suburb" class="customDropDown">
                <option value="">Select...
                <?php
                  try {
                    //Populate select input with unique suburbs from the database
                    $stmt = $con->query('SELECT DISTINCT SUBURB FROM parks ORDER BY SUBURB ASC');
                    $result = $stmt -> fetchAll();

                    foreach( $result as $suburb ) {
                      echo '<option value="',$suburb['SUBURB'],'">',$suburb['SUBURB'],'</option>'; //create each select option
                    }
                  }
                  catch(PDOException $ex) {
                    echo "Error: " . $ex; //user friendly message
                  }
                ?>
              </select>
            <br />
            <br />
          </div>
            <input class="submitButton" type="submit" value="Submit">
          </form>
        </div>
        <!--Name Form -->
        <div id="nameSearch" style="display: none">
          <form name="myForm" action="results.php" onsubmit="return SearchByName();">
            <div class="searchboxText">
            <input type="text" class="customTextbox" name="name" placeholder="Enter the Name of a Park">
            <br />
            <br />
          </div>
            <input class="submitButton" type="submit" value="Submit">
          </form>
        </div>
        <!--Rating Form -->
        <div id="ratingSearch" style="display: none">
          <form action="results.php" onsubmit="return checkRating();">
            <div class="searchboxText">
              <select id="rating" name="rating" class="customDropDown">
				        <option value="">Select Rating...</option>
                <option value="1">1 Star</option>
                <option value="2">2 Stars</option>
                <option value="3">3 Stars</option>
                <option value="4">4 Stars</option>
                <option value="5">5 Stars</option>
              </select>
            <br />
            <br />
          </div>
            <input class="submitButton" type="submit" value="Submit">
          </form>
        </div>
        <!--Geo Form -->
        <div id="geoSearch" style="display: none">
          <form action="results.php" name="geoSearchForm">
            <p>
              <input id="lat" type="hidden" name="lat">
              <input id="long" type="hidden" name="long">
            </p>
            <p>
            <input class="submitButton" type="submit" value="Locate">
          </p>
          </form>
        </div>
        <br />
      </div>
    </div>
    <footer><p>Patrick Freeman and Nicholas Mulrine for QUT CAB230 2016<p></footer>
  </body>
</html>
