<?php
session_start();

//remove session variables
unset($_SESSION['loggedin']);
unset($_SESSION['id']);
header('Location:index.php');
?>
