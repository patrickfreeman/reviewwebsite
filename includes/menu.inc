<div id="menu">
  <ul>
    <li><a href="index.php">Home</a></li>
    <?php
    if(isset($_SESSION['loggedin'])) {
      echo '<li><a href="logout.php">Log Out</a></li>';
    } else {
      echo '<li><a href="login.php">Log In</a></li>';
    }
    ?>
  </ul>
</div>
