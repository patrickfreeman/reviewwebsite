<!DOCTYPE html>
<html>
  <head>
    <?php
      session_start();
      include 'includes/config.php';
    ?>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script>
    var latitudes = [];
    var longitudes = [];
    </script>
    <title>Park Search - Results</title>
  </head>
  <body>
    <div id="header">
        <?php include "includes/menu.inc"; ?>
    </div>
    <div id="wrapper">
      <div id="results">
        <h1>Results</h1>
        <div id="demo">
        </div>
        <?php
        $num_results = 10;
        $results = Array();
        //Query for searching by subrub
        if(isset($_GET['suburb'])) {
          try {
            $stmt = $con->prepare('SELECT * FROM parks WHERE Suburb = :suburb LIMIT :results');
            $stmt->bindValue(':suburb', $_GET['suburb']);
            $stmt->bindValue(':results', $num_results);
            $stmt->execute();
            $results = $stmt -> fetchAll();
          }
          catch(PDOException $ex) {
            echo "Error: " . $ex; //user friendly message
          }
        }
        //Query for searching by name of park
        else if(isset($_GET['name'])) {
          try {
            $stmt = $con->prepare('SELECT * FROM parks WHERE Name LIKE :name LIMIT :results');
            $stmt->bindValue(':name', '%'.$_GET['name'].'%');
            $stmt->bindValue(':results', $num_results);
            $stmt->execute();
            $results = $stmt -> fetchAll();
          }
          catch(PDOException $ex) {
            echo "Error: " . $ex; //user friendly message
          }
        }
        //Query for searching by rating
        else if(isset($_GET['rating'])) {
          try{
            $stmt = $con->prepare('SELECT * FROM parks WHERE rating >= :rating ORDER BY rating LIMIT :results');
            $stmt->bindValue(':rating', $_GET['rating']);
            $stmt->bindValue(':results', $num_results);
            $stmt->execute();
            $results = $stmt -> fetchAll();
          }
          catch(PDOException $ex) {
            echo "Error: " . $ex; //user friendly message
          }
        }
        //Query for searching by geolocation
        else if(isset($_GET['lat']) && isset($_GET['long'])) {
          try {
            //compares two lat and long values and gives a distance.
            $stmt = $con->prepare(
            'SELECT *,
            ( 3959 * acos( cos( radians( :lat ) ) *
            cos( radians( Latitude ) ) *
            cos( radians( Longitude ) - radians( :long ) ) +
            sin( radians( :lat2 ) ) *
            sin( radians( Latitude ) ) ) )
            AS distance FROM parks HAVING distance < 5 ORDER BY distance LIMIT 0 , 20'
            );

            $stmt->bindValue(':lat', $_GET['lat']);
            $stmt->bindValue(':lat2', $_GET['lat']);
            $stmt->bindValue(':long', $_GET['long']);
            $stmt->execute();
            $results = $stmt -> fetchAll();
          }
          catch(PDOException $ex) {
            echo "Error: " . $ex; //user friendly message
          }
        }

        //set amount of results depending on number of results
        if ($num_results > count($results)) {
          $num_results = count($results);
        }
        //if no parks
        if (count($results) == 0) {
          echo "Sorry, we couldn't find any parks :(";
        }

        //loop for displaying each result
        for ($i = 0; $i < $num_results; $i++) {

          //pushing php variable into javascript array
          echo "
          <script>
          latitudes.push(" . $results[$i]['Latitude'] . ");
          longitudes.push(" . $results[$i]['Longitude'] . ");
          </script>
          ";

          echo '<div class="row">';
          echo '<div class="imgThumb">';
          echo '</div>';
            echo '<div class="rowTitle">';
              echo '<a href="itempage.php?id='. $results[$i]['id'].'"><p>' . $results[$i]['Name'] .'</p></a>';
            echo '</div>';
            echo '<div class="resultDesc">';
              echo
               '<p>Park Description
               </p>';
            echo '</div>';
            echo '<div class="resultInfo">';
              echo '<table>';
                echo '<tr>';
                  echo '<td><b>Street:</b> </td>';
                  echo '<td>' . $results[$i]['Street'] .'</td>';
                echo '</tr>';
                echo '<tr>';
                  echo '<td><b>Suburb:</b> </td>';
                  echo '<td>' . $results[$i]['Suburb'] .'</td>';
                echo '</tr>';
                echo '<tr>';
                  echo '<td><b>Rating: </b></td>';
                  if ($results[$i]['rating'] > 0) {
                    echo '<td>' . round($results[$i]['rating'],1) .'/5</td>';
                  } else {
                    echo '<td>No Rating</td>';
                  }
                echo '</tr>';
                if (isset($results[$i]['distance'])) {
                  echo '<tr>';
                    echo '<td><b>Distance:</b> </td>';
                    echo '<td>' . round($results[$i]['distance'],3) .' km</td>';
                  echo '</tr>';
              }
              echo '</table>';
            echo '</div>';
          echo '</div>';
        }
        ?>

      </div>
    </div>
    <footer><p>Patrick Freeman and Nicholas Mulrine for QUT CAB230 2016<p></footer>
      <script src ="js/resultsMap.js"> </script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh97TYQpVen_cp82xsPrIeu8RPEC3W0_4&callback=initMap"
      async defer></script>
  </body>

</html>
