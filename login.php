<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
	<script src="js/validation.js"></script>
    <title>Park Search - Login</title>
  </head>
  <body>
    <div id="headerthin">
        <?php
          session_start();
          include "includes/menu.inc";
        ?>
    </div>
    <div id="wrapper">
      <div class="login-page">
        <div class="loginform">
          <h3>Login</h3>
          <form name="login-form" class="login-form" action="loginsubmit.php" method="post" onsubmit="return LogIn();">
            <input id="username" name="username" type="text" placeholder="username or email"/>
            <input id="password" name="password" type="password" placeholder="password"/>
            <div class ="incorrectInput"><span id="errorSpanLogin">test</span></div>
            <?php
              if(isset($_GET['fail'])){
                echo '<div class ="incorrectInput" style="visibility: visible"><span id="errorSpanLogin">Incorrect Login</span></div>';
              }
            ?>
            <input class="submitButton" type="submit" value="Log In" style="background: #4CAF50">
            <p class="message">Not signed up? Create an account below</p>
          </form>
          <br />
          <h3>Sign Up</h3>
          <form name="register-form" class="register-form" action="SignupSubmit.php" method="post" onsubmit="return SignUp();">
            <input id="username2" name="username2" type="text" placeholder="username"/>
            <input id="password2" name="password2" type="password" placeholder="password"/>
            <input id="email" name="email" type="text" placeholder="email address"/>
            <p>
              <label>Gender: </label>
            <input style="width: 10%" type="radio" name="gender" value="male"> Male
            <input style="width: 10%" type="radio" name="gender" value="female"> Female
            <input style="width: 10%" type="radio" name="gender" value="other"> Other
          </p>
          <input style="width: 10%" type="checkbox" name="recemail" value="1"> Receive Emails?
          <div class ="incorrectInput"><span id="errorSpanSignup">test</span></div>
          <?php
            if(isset($_GET['signup'])){
              if($_GET['signup'] == true) {

              } else {
                echo '<div class ="incorrectInput" style="visibility: visible"><span id="errorSpanLogin">Error On Signu</span></div>';
              }
            }
          ?>
            <input class="submitButton" type="submit" value="Sign Up" style="background: #4CAF50">
            <p class="message"><a href="index.php">Home</a></p>
          </form>
        </div>
      </div>
    </div>
    <footer><p>Patrick Freeman and Nicholas Mulrine for QUT CAB230 2016<p></footer>
  </body>
</html>
