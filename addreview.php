<?php
  //This page use used to submit reviews to the database.
  session_start();
  include 'includes/config.php'; //db connection
  $valid = true;
  //check result id
  $idRegex = "^[0-9]+$";
  if(!ereg($idRegex,$_SESSION['resultid'])) {
    $valid = false;
  }
  //check uid
  if(!ereg($idRegex,$_SESSION['id'])) {
    $valid = false;
  }
  //check review
  if (!$_POST["review"] != "") {
    $valid = false;
  }
  //check rating
  if ($_POST["review"] == '1' || $_POST["review"] == '2'
  ||$_POST["review"] == '3' ||$_POST["review"] == '4' || $_POST["review"] == '5') {
    echo "review test<br>";
  }
  //if data is valid do db things
  if ($valid) {
    try {
      $id = $_SESSION['resultid']; //get page result id
      $uid = $_SESSION['id']; //get user id
      $stmt = $con->prepare("INSERT INTO reviews (parkid, userid, review, rating)
      VALUES (:id, :uid, :review, :rating)");
    	$stmt->bindValue(':id', $id);
    	$stmt->bindValue(':uid', $uid);
      $stmt->bindValue(':review', $_POST["review"]);
    	$stmt->bindValue(':rating', $_POST["rating"]);
    	$stmt->execute();

    } catch(PDOException $ex) {
        echo "Error: " . $ex; //user friendly message
    }
  } else {
    echo "not valid";
  }
  header("Location:itempage.php?id=$id"); //set page back to same park id page
?>
