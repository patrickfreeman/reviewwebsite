<?php
	include "includes/config.php"; //db connection
	if (isset($_POST['username']) && isset($_POST['password'])){ // check username & password exists
		$password = md5($_POST["password"]); //convert password to hashed password to compare with db

		if (checkPassword($_POST['username'], $password))
		{
			session_start();
			try {
				$stmt = $con->prepare("SELECT userid from users WHERE username = :username");
				$stmt->bindValue(':username', $_POST['username']);
				$stmt->execute();
				$results = $stmt->fetchAll();
				$_SESSION['loggedin'] = true; // set logged in status session variable
				$_SESSION['id'] = $results[0]['userid']; //set user id session variable
				header('Location:index.php');
			}
			catch(PDOException $ex) {
				echo "Error: " . $ex; //user friendly message
			}
		}
		else
		{
			header("Location:login.php?fail=true");
		}

	}
	//returns true if a record exists with $username and $password
	function checkPassword($username, $password) {

		include "includes/config.php"; //db connection
		try {
			$sql1 = $con->prepare("SELECT * FROM users WHERE username = :username and password = :password");
			$sql1->bindValue(':username',$username);
			$sql1->bindValue(':password',$password);
			$sql1->execute();

			//make sure a result exists
			if ($sql1->rowCount() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch(PDOException $ex) {
			echo "Error: " . $ex; //user friendly message
		}

	}

?>
