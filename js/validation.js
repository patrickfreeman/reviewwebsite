//Run when searching for a park by name
function SearchByName() {
    var x = document.forms["myForm"]["name"].value;
    var regex = /^[a-zA-Z 0-9]+$/;

    //Check if the name box if left empty upon clicking search
    if (x == null || x == "") {
        //If the name box if left empty upon clicking search
        document.getElementById("errorSpan").innerHTML = "Please enter a name";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }
    //Check if the name box contains only leters
    if (!x.match(regex)) {
        //if the name box does not contains only leters and numbers
        document.getElementById("errorSpan").innerHTML = "Please use only letters and numbers";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }

}

//Run when searching for a park by state
function checkSuburb() {
    var regex = /^[a-zA-Z ]+$/;

    //Check if a state has been selected
    if (suburb.value == "") {
        //If no state has been selected
        document.getElementById("errorSpan").innerHTML = "Please Select a Suburb";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    } else if (!suburb.value.match(regex)) {
        //if the suburb box does not contains only leters
        document.getElementById("errorSpan").innerHTML = "Incorrect Suburb";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }
}

//Run when searching for a park by rating
function checkRating() {
    var regex = /^[0-9]$/;
    //Check if a rating has been selected
    if (rating.value == "") {
        //If no rating has been selected
        document.getElementById("errorSpan").innerHTML = "Please Select a Rating";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    } else if (!rating.value.match(regex)) {
        //if the rating box does not contain one number
        document.getElementById("errorSpan").innerHTML = "Incorrect Rating";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }
}

//Run when logging in
function LogIn() {
    var x = document.forms["login-form"]["username"].value;
    var y = document.forms["login-form"]["password"].value;

    //if the username box is empty upon clicking login
    if (x == null || x == "") {
        //if the password box is empty upon clicking login
        document.getElementById("errorSpanLogin").innerHTML = "Please Enter a Username";
        document.getElementById("errorSpanLogin").style.visibility = "visible";
        return false;
    }

    //Check if the password box is empty upon clicking login
    if (y == null || y == "") {
        //if the password box is empty upon clicking login
        document.getElementById("errorSpanLogin").innerHTML = "Please Enter a Password";
        document.getElementById("errorSpanLogin").style.visibility = "visible";
        return false;
    }
}

//When signing up
function SignUp() {
    var username = document.forms["register-form"]["username2"].value;
    var password = document.forms["register-form"]["password2"].value;
    var email = document.forms["register-form"]["email"].value;
    var gender = document.forms["register-form"]["gender"].value;
    var recemail = document.forms["register-form"]["recemail"].value;
    var emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i

    if (username == null || username == "") {
        document.getElementById("errorSpanSignup").innerHTML = "Please Enter a Username";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        return false;
    }
    if (password == null || password == "") {
        document.getElementById("errorSpanSignup").innerHTML = "Please Enter a Password";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        return false;
    }
    //Check if the email address box is empty upon clicking login
    if (email == null || email == "") {
        //If the email address box is empty upon clicking login
        document.getElementById("errorSpanSignup").innerHTML = "Please Enter an Email Address";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        //document.getElementById("username").focus();
        return false;
    }
    //Check if email address enetered in login box is valid
    else if (!email.match(emailRegex)) {
        //if email address entered is not valid
        document.getElementById("errorSpanSignup").innerHTML = "Please Enter a Valid Email Address";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        return false;
    }

    //Check if the email address box is empty upon clicking login
    if (gender != "male" || gender != "female" || gender != "other") {
        document.getElementById("errorSpanSignup").innerHTML = "Please Select a gender";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        return false;
    }

    //Check if the email address box is empty upon clicking login
    if (recemail != 1 || recemail != "" || recmail != null) {
        document.getElementById("errorSpanSignup").innerHTML = "Incorrect Receive email value";
        document.getElementById("errorSpanSignup").style.visibility = "visible";
        return false;
    }
}

//When submitting a review up
function SubmitReview() {
    var rating = document.forms["reviewForm"]["rating"].value;
    var review = document.getElementById("review").value;

    //Rating needs to be from 1-5
    if (rating != '1' && rating != '2' && rating != '3' && rating != '4' && rating != '5') {
        document.getElementById("errorSpan").innerHTML = "Incorrect Rating";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }

    //Review cant be empty
    if (review == '') {
        document.getElementById("errorSpan").innerHTML = "Please Enter a Review";
        document.getElementById("errorSpan").style.visibility = "visible";
        return false;
    }
}
