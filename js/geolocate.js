//gets users location
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        document.forms["geoSearchForm"]["lat"].value = "Geolocation is not supported by this browser.";
    }
}
//sets a value so it can be posted for further processing
function showPosition(position) {
    document.forms["geoSearchForm"]["lat"].value = position.coords.latitude;
    document.forms["geoSearchForm"]["long"].value = position.coords.longitude;
}
