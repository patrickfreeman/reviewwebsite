//These functions show and hide the respective forms for searching on index.php
function suburbForm() {
  suburbSearch.style.display = 'inline';
  nameSearch.style.display = 'none';
  ratingSearch.style.display = 'none';
  geoSearch.style.display = 'none';
  document.getElementById("errorSpan").style.visibility = "hidden";
}

function nameForm() {
  suburbSearch.style.display = 'none';
  nameSearch.style.display = 'inline';
  ratingSearch.style.display = 'none';
  geoSearch.style.display = 'none';
  document.getElementById("errorSpan").style.visibility = "hidden";
}

function geoForm() {
  suburbSearch.style.display = 'none';
  nameSearch.style.display = 'none';
  ratingSearch.style.display = 'none';
  geoSearch.style.display = 'inline';
  document.getElementById("errorSpan").style.visibility = "hidden";
}

function ratingForm() {
  suburbSearch.style.display = 'none';
  nameSearch.style.display = 'none';
  ratingSearch.style.display = 'inline';
  geoSearch.style.display = 'none';
  document.getElementById("errorSpan").style.visibility = "hidden";
}
