//used to display a thumbnail sized map on each result of the results.php page.
var map;
function initMap() {
  var divs = document.getElementsByClassName("imgThumb");

  for (i = 0; i < divs.length; i++) {
    var latitude = latitudes[i];
    var longitude = longitudes[i];
    var myLatLng = {lat: latitude, lng: longitude};
    var map = new google.maps.Map(divs[i], {
      zoom: 16,
      center: myLatLng,
      disableDefaultUI: true
    });

    var marker = new google.maps.Marker({
      position: myLatLng,
      animation: google.maps.Animation.DROP,
      map: map,
    });
  }
}
