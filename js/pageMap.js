//used to display a map on an individual park page
var map;
function initMap() {
  var myLatLng = {lat: lat, lng: long};

  var contentString = "<b>" + name + "</b>" + "<br> Lattitude: " + lat + "<br> Longitude: " + long
    + "<br> Suburb: " + suburb + "<br> Street: " + street;

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 16,
    center: myLatLng,
    disableDefaultUI: true
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Hello World!'
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });
}
