<?php
// this page submits user sign up data to the database
if (isset($_POST["username2"]) && isset($_POST["password2"])
	&& isset($_POST["email"]) && isset($_POST["gender"])){
		$valid = true;

	session_start();
	include "includes/config.php"; //db connection

	//Check username
	if($_POST["username2"] == null || $_POST["username2"] == "") {
		$valid = false;
	}
	//Check password
	if($_POST["password2"] == null || $_POST["password2"] == "") {
		$valid = false;
	}
	//check email
	if($_POST["email"] == null || $_POST["email"] == "") {
		$valid = false;
	}
	$emailRegex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
	if(!ereg($emailRegex,$_POST["email"])) {
		$valid = false;
	}
	//check gender
	if($_POST["gender"] != "male" && $_POST["gender"] != "female" && $_POST["gender"] != "other") {
		$valid = false;
	}

	if ($valid) {
	  try {
			$password = md5($_POST["password2"]); //hash password function
		  $stmt = $con->prepare("INSERT INTO users (username, email, password, gender, recemail)
			VALUES (:username, :email, :password, :gender, :recemail)");

			$stmt->bindValue(":username", $_POST["username2"]);
		  $stmt->bindValue(":email", $_POST["email"]);
			$stmt->bindValue(":password", $password); //store hashed password
			$stmt->bindValue(":gender", $_POST["gender"]);
			$stmt->bindValue(":recemail", $_POST["recemail"]);
			$stmt->execute();

			header("Location:login.php?signup=true");

	  } catch(PDOException $ex) {
	      echo "Error: " . $ex; //user friendly message
	  }
	} else {
		header("Location:login.php?signup=false");
	}
}

?>
