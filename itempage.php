<!DOCTYPE html>
<html>
  <head>
    <?php
    session_start();
    include 'includes/config.php'; //db connection
    try {
      //get result from db where id is equal to the post id
      $stmt = $con->prepare('SELECT * FROM PARKS WHERE id=:id');
      $stmt->bindValue(':id', $_GET['id']);
      $stmt->execute();
      $result = $stmt->fetchAll();
      //make sure park exists and set variable
      if($stmt->rowCount() == 1) {
        $park =  $result[0];
        $_SESSION['resultid'] = $park['id'];
      }

    } catch (PDOException $ex) {
      echo "Error: " . $ex;
    }

    try {
      //get average
      $stmt = $con->prepare('SELECT AVG(rating) AS average
        FROM reviews WHERE parkid = :id');
      $stmt->bindValue(':id', $_GET['id']);
      $stmt->execute();
      $averages = $stmt->fetchAll();
      if($stmt->rowCount() == 1) {
        $average =  $averages[0];
      }
      //put average in the result park.rating value
      $stmt = $con->prepare('UPDATE parks SET rating= :rating WHERE id= :id');
      $stmt->bindValue(':rating', $average['average']);
      $stmt->bindValue(':id', $_GET['id']);
      $stmt->execute();

    } catch (PDOException $ex) {
        echo "Error: " . $ex;
      }
    ?>
    <link rel="stylesheet" type="text/css" href="style/style.css">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <script src="js/validation.js"></script>
    <title>Park Search - <?php echo $park['Name'];?></title>
  </head>
  <body>
    <div id="header">
        <?php include 'includes/menu.inc'; ?>
    </div>
    <div id="wrapper">
      <div id="content">
        <div class="contentText">
          <h1><?php echo $park['Name'];?></h1>
          <p>
            Park Description paragraph would go here if I had the data for it.  Park Description paragraph would go here if I had the data for it.  Park Description paragraph would go here if I had the data for it.
          </p>
          <h3> Details: </h3>

          <?php
            echo "<p><b>Street:</b> " . $park['Street'] . "</p>";
            echo "<p><b>Suburb:</b> " . $park['Suburb'] . "</p>";
            if (isset($average['average'])) {
            echo "<p><b>Rating:</b> " . $average['average'] . "</p>";
            } else {
              echo "<p><b>Rating:</b> No Ratings</p>";
            }
            //code for map lat and long, php -> js variable conversion
            echo "<script>
            var lat = " . $park['Latitude'] .";
            var long = " . $park['Longitude'] .";
            var name = '" . $park['Name'] ."';
            var suburb = '" . $park['Suburb'] ."';
            var street = '" . $park['Street'] ."';
            </script>";
          ?>
        </div>
        <div id="map" class="contentImage">
        </div>
    </div>
    <div class="commentbox">
        <h1>Reviews</h1>
    <?php
        try {
          $num_results = 5;
          $stmt = $con->prepare('SELECT * FROM reviews WHERE parkid=:parkid
          ORDER BY YEAR(date) DESC, MONTH(date) DESC, DAY(date) DESC');
          $stmt->bindValue(':parkid', $_GET['id']);
          $stmt->execute();
          $results = $stmt->fetchAll();
          if($stmt->rowCount() > 0) {
            //output commentbox
            //get 3 most recent reviews
            //loop each review into box

            if ($num_results > count($results)) {
              $num_results = count($results);
            }

            $join = $con ->prepare(
            'SELECT username
            FROM users
            INNER JOIN reviews
            ON reviews.userid=users.userid
            WHERE id = :id');

            for ($i = 0; $i < $num_results; $i++) {
              $join->bindValue(':id', $results[$i]['id']);
              try {
              $join->execute();
              $joined = $join->fetchAll();
              } catch (PDOException $e) {
                echo $e;
              }
                  echo '<div class="rowcomment">';
                    echo '<div class="rowTitle">';
                      echo '<h3>'. $joined[0][0] .' - Rating '. $results[$i]['rating'] .'/5</h3>';
                        echo '<div class="commentIndividual">';
                          echo "<p>" . $results[$i]['review'] . "</p>";
                        echo '</div>';
                    echo '</div>';
                  echo '</div>';
            }
          }

          else {
            echo "There are no reviews for this park";
          }
        } catch (PDOException $ex) {
          echo "Error: " . $ex;
        }
     ?>
     </div>

     <?php
        #if logged in show the submit review box
        if (!isset($_SESSION['loggedin'])) {
          echo '<div class="commentbox" style="visibility: hidden">';
        } else {
          echo '<div class="commentbox">';
        }
     ?>
        <div class="sendComment">
          <h1>Send A Review</h1>
          <div class ="incorrectInput"><span id="errorSpan">test</span></div>
        <form name="reviewForm" action="addreview.php" onsubmit="return SubmitReview();" method="post" >
          <table style="width: 100%" >
          	<tr>
          		<td>Rating: </td>
          		<td>
                <select name="rating" class="customDropDown" style="width:30%">
                  <option value="1">1 Star</option>
                  <option value="2">2 Stars</option>
                  <option value="3">3 Stars</option>
                  <option value="4">4 Stars</option>
                  <option value="5">5 Stars</option>
                </select>
              </td>
          	</tr>
          	<tr>
          		<td>Comment: </td>
          		<td><textarea id="review" name="review" rows="5"></textarea><br><br></td>
          	</tr>
          </table>
          <input class="submitComment" type="submit" name="submit" value="Submit">
        </form>
      </div>
      </div>

    </div>
    <footer><p>Patrick Freeman and Nicholas Mulrine for QUT CAB230 2016<p></footer>
      <script src ="js/pageMap.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh97TYQpVen_cp82xsPrIeu8RPEC3W0_4&callback=initMap"
      async defer></script>
  </body>
</html>
